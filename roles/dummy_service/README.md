# USAGE
ansible-playbook plays/dummy_app.yml


# DIAGRAM

        ansible       manager         workers
        |               |               |
        |               |               |
        |-------------->+               |       *deploy docker service on cluster
        |               |               |
        |               |-------------->+
        |               |<--------------|
        |               |               |
        |<--------------|               |
        |               |               |
        |               |               |
        V               V               V

