# USAGE
ansible-playbook plays/init_swarm.yml


# DIAGRAM

        ansible      group_vars      group_vars       group_vars
        |            managers[0]     manager[1:]      workers
        |               |               |               |
        |               |               |               |
        |-------------->+               |               |       *initialize docker swarm cluster on
        |<--------------|               |               |        in inventory group_vars 'managers'
        |               |               |               |
        |               |               |               |
        |------------------------------>+               |       *make other managers join swarm cluster
        |<------------------------------|               |
        |               |               |               |
        |               |               |               |
        |---------------------------------------------->+       *make workers join swarm cluster
        |<----------------------------------------------|
        |               |               |               |
        |               |               |               |
        V               V               V               V

