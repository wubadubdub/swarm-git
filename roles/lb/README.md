# USAGE
ansible-playbook plays/lb_app.yml


# DIAGRAM

        ansible        lb 
        |               |
        |               |
        |-------------->+       *deploy LB config.
        |<--------------|        the service is exposed by the swarm managers as backends for the LB.
        |               |
        |               |
        V               V

                               /etc/nginx/conf.d/dummy.conf:

                                 upstream docker_managers {
                                 {% for manager in manager_list %}
                                   server {{ manager }}:{{ app.published_port }} weight=1;
                                 {% endfor %}
                                 }
                                 
                                 server {
                                   listen 80;
                                   server_name _;
                                   location / {
                                     proxy_pass http://docker_managers/ ;
                                   }
                                 }
                                 


        user           lb            managers
        |               |               |
        |               |               |
        |-------------->+               |       *user consumes docker service via LB
        |               |               |
        |               |-------------->+
        |               |<--------------|
        |               |               |
        |<--------------|               |
        |               |               |
        |               |               |
        |               |               |
        V               V               V

