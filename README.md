# OVERVIEW

     .
     ├── inventory              step 1: define your inventory           
     ├── plays
     │   ├── init_swarm.yml     step 2: install docker and configure swarm cluster
     │   ├── dummy_app.yml      step 3: deploy a service on the cluster
     │   └── lb_apps.yml        step 4: configure LB to expose service to users
     
     
See roles' README.md for playbook execution and further details


# INFRASTRUCTURE DIAGRAM

                ansible (SSH: TCP/22).
                        .
                         .
                          .
                           .
                            .
                             .
                              . worker ........... manager
                             .         .       .
                            .           .     .
                           .             .   .
                          .               . .
                         .                 .
     user ..... LB  .....                 . .
     (HTTP TCP/80)       .               .   .
                          .             .     .
                           .           .       .
                            .         .         .
                             .       .           .
                              . worker-n ........... manager-n


